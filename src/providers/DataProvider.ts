export interface DataProvider {
    create: Function
    read: Function
    update: Function
    delete_: Function
}