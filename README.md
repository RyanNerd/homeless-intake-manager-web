# pantry-intake
Pantry Intake web portal for Switchpoint (but could be used for any charitable organization food pantry)

### 💾 Installation
There are two parts to pantry-intake:

`pantry-intake-web`: Which is this repo and is the web or client.
[`pantry-intake-app`](https://github.com/RyanNerd/pantry-intake-app): This is the web service app.

You can clone these repos with:

`git clone RyanNerd/pantry-intake-web`
`git clone RyanNerd/pantry-intake-app`

`cd pantry-intake-web`
`npm install`

Look at the `.env-example` and create a `.env` file that matches the example configuration.
Once you have set up your `.env` you can run a dev web server with:

`npm run dev-server`

Follow the installation instructions [here](https://github.com/RyanNerd/pantry-intake-app/blob/master/README.md) to set up the pantry web service.